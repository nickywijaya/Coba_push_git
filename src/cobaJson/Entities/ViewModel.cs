﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cobaJson.Entities
{
    public class ViewModel
    {
        public int BookId { get; set; }
        public string Name { get; set; }
        public Content Content { get; set; }
    }
}
