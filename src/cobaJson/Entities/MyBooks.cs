using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cobaJson.Entities
{
    public class MyBooks
    {
        [Key]
        public int BookId { get; set; }

        public string Name { get; set; }

        public int Content { get; set; }

    }
}


