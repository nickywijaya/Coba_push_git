using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cobaJson.Entities
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

        }
        public virtual DbSet<jsonEntities> Logging { get; set; }
        public virtual DbSet<MyBooks> MyBooks { get; set; }
        public virtual DbSet<Content> Content { get; set; }
    }
}