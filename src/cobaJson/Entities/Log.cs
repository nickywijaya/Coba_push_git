﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cobaJson.Entities
{
    public class Log
    {
        public DateTime Timestamp { get; set; }
        public String MessageTemplate { get; set; }
        public string Level { get; set; }
        public string Properties { get; set; }
    }
}
