﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace cobaJson.Entities
{
    public class Content
    {
        [Key]
        public int ContentId { get; set; }
        public string Bab1 { get; set; }
        public string Bab2 { get; set; }
        public string Bab3 { get; set; }
    }
}
