﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using cobaJson.Entities;
using Microsoft.EntityFrameworkCore;
using Dapper;
using System.IO;
using Newtonsoft.Json;
using cobaJson.Services;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace cobaJson.Controllers
{
    [Route("api/[controller]")]
    public class BookController : Controller
    {
        private readonly ApplicationContext _a;
        public BookController(ApplicationContext a)
        {
            this._a = a;
        }
        // GET: api/values
        [HttpGet]
        public IActionResult Get()
        {


            //models.Content = new Content();
            //            models = _a.Database.GetDbConnection().Query<ViewModel>(@"
            //            SELECT BookId as BookId, Name, Content, C.Bab1, C.Bab2, C.Bab3
            //            FROM MyBooks M join Content C on M.Content = C.ContentId
            //").FirstOrDefault();

            //var x = _a.MyBooks.FirstOrDefault();

            //var s = JsonConvert.SerializeObject(x);

            //var y = JsonConvert.DeserializeObject<MyBooks>(s);

            //y.BookId = 3;

            //_a.MyBooks.Add(y);
            //_a.SaveChanges();
            
            var newLog = new List<Log>();
            string s = "";
            var path = @"C:\Users\Nicky\Documents\Visual Studio 2015\Projects\cobaJson\src\cobaJson\SampleLog.json";
            using (var reader = new StreamReader(path))
            {
                //read all json and convert them to string
                string content = reader.ReadToEnd();
                s = JsonConvert.DeserializeObject(content).ToString();

                //split by properties, so let properties a raw json string, othwerwise bind to model
                string[] words = s.Split(new string[] { "\"Properties\"" }, StringSplitOptions.None);

                //setting ready words[0] go to json and bind to model
                words[0] = StringService.ReplaceAt(words[0], words[0].LastIndexOf(','), 1, "}");

                //setting ready words[1] to the raw jsonString and save it into string format
                words[1] = words[1].Insert(0, "{\"Properties\"");

                //binding to model
                jsonEntities JsonModel = new jsonEntities();
                JsonModel = JsonConvert.DeserializeObject<jsonEntities>(words[0]);
                JsonModel.Properties = words[1];

                //save to DB
                _a.Logging.Add(JsonModel);
                _a.SaveChanges();

            }

           // var models = _a.Content.FirstOrDefault(Q => Q.ContentId == 100).Bab1;

            //var jsonStrings = JsonConvert.SerializeObject(models);
            return Ok(s);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        
    }
}
